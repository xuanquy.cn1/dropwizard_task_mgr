package com.dropwizardexample.core;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.sql.Timestamp;

public class Task {
    @JsonProperty
    private long id;

    @JsonProperty
    private long user_id;

    @JsonProperty
    private String name;

    @JsonProperty
    private String content;

    @JsonProperty
    private int status;

    @JsonProperty
    private Timestamp dead_line;

    @JsonProperty
    private Timestamp created_at;

    @JsonProperty
    private Timestamp updated_at;

    public long getId() {
        return id;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Timestamp getDead_line() {
        return dead_line;
    }

    public void setDead_line(Timestamp dead_line) {
        this.dead_line = dead_line;
    }

    public Timestamp getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Timestamp created_at) {
        this.created_at = created_at;
    }

    public Timestamp getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Timestamp updated_at) {
        this.updated_at = updated_at;
    }

    @Override
    public String toString() {
        String str = "";
        str += Long.toString(user_id);
        str += " " + name;
        str += " " + content;
        str += " " + status;
        str += " " + dead_line.toString();
        str += " " + created_at.toString();
        str += " " + updated_at.toString();
        return str;
    }
}
