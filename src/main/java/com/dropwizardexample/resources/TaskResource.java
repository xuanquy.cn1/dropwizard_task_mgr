package com.dropwizardexample.resources;

import com.codahale.metrics.annotation.Timed;
import com.dropwizardexample.api.Message;
import com.dropwizardexample.core.Task;
import com.dropwizardexample.db.TaskDAO;
import io.dropwizard.jersey.params.IntParam;
import io.dropwizard.jersey.params.LongParam;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public class TaskResource {

    private final TaskDAO dao;
    public TaskResource(TaskDAO myDAO){
        this.dao = myDAO;

    }
    @POST
    @Timed
    @Path("/task")
    public Message Create(@QueryParam("user_id") long user_id, @QueryParam("name") Optional<String> name, @QueryParam("content") Optional<String> content, @QueryParam("dead_line") Long dead_line){
        int status =1;
        Timestamp currentTime = new Timestamp(System.currentTimeMillis());
        dao.insert(user_id,name.get(),content.get(),status, new Timestamp(dead_line), currentTime, currentTime);
        return new Message(20, "Create Task success!");
    }

    @GET
    @Timed
    @Path("/tasks")
    public List<Task> getAll(){
        List<Task> tasks= dao.fetchAll();
        String str = "";
        for (int i =0 ; i < tasks.size(); i++){
            str += tasks.toString();
            str += "<br/>";

        }
        return tasks;
    }
    @GET
    @Timed
    @Path("/tasks/{status}")
    public List<Task> getByStatus(@PathParam("status") int status){
        List<Task> tasks= dao.by_status(status);
        String str = "";
        for (int i =0 ; i < tasks.size(); i++){
            str += tasks.toString();
            str += "<br/>";

        }
        return tasks;
    }

    @GET
    @Timed
    @Path("/task/{id}")
    public Task getById(@PathParam("id") LongParam id){
        Task task= dao.get(id.get());
        String str = task.toString();
        return task;
    }

    @DELETE
    @Timed
    @Path("/task/{id}")
    public Message delete(@PathParam("id") LongParam id){

        try {
            dao.delete(id.get());
        }catch (Exception e){
            e.printStackTrace();
            return new Message(20, "Delete Task failure!");
        }
        return new Message(20, "Delete Task success!");
    }
    @POST
    @Timed
    @Path("/task/{id}/{status}")
    public Message updateStatus(@PathParam("id") long id, @PathParam("status") int status){
        Timestamp currentTime = new Timestamp(System.currentTimeMillis());
        try {
            dao.update_status(id, status,currentTime);
        }catch (Exception e){
            e.printStackTrace();
            return new Message(20, "Update Task failure!");
        }
        return new Message(20, "Update Task success!");
    }

}
