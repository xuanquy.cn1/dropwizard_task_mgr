package com.dropwizardexample.resources;

import com.codahale.metrics.annotation.Timed;
import com.dropwizardexample.api.Message;
import com.dropwizardexample.db.UserDAO;
import com.dropwizardexample.core.User;
import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.jersey.params.LongParam;
import org.jdbi.v3.core.Jdbi;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public class UserResource   {

    private final UserDAO dao;
    public UserResource(UserDAO myDAO){
        this.dao = myDAO;

    }
    @POST
    @Timed
    @Path("/user")
    public Message Create(@QueryParam("fullname") String fullname, @QueryParam("birthday") Long birthday, @QueryParam("jobtitle") Optional<String> jobtitle){
        dao.insert(fullname, new Timestamp(birthday), jobtitle.get());
        return new Message(20, "Create User success!");
    }

    @GET
    @Timed
    @Path("/users")
    public List<User> getAll(){
        List<User> users= dao.get_all();
        return users;
    }
}
