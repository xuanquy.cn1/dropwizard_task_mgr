package com.dropwizardexample.db;

import com.dropwizardexample.core.Task;
import com.dropwizardexample.core.TaskMapper;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.statement.UseRowMapper;

import java.sql.Timestamp;
import java.util.List;

public interface TaskDAO {

    @SqlQuery("select * from task")
    @UseRowMapper(TaskMapper.class)
    List<Task> fetchAll();

    @SqlUpdate("insert into task ( user_id, name, content, status , dead_line, created_at, updated_at) values ( :user_id, :name, :content, :status , :dead_line, :created_at, :updated_at)")
    void insert(@Bind("user_id") long user_id, @Bind("name") String name, @Bind("content") String content,@Bind("status") int status,  @Bind("dead_line") Timestamp dead_line,  @Bind("created_at") Timestamp create_at,  @Bind("updated_at") Timestamp update_at);

    @SqlQuery("select * from task where id = :id ")
    @UseRowMapper(TaskMapper.class)
    Task get(@Bind("id") long id);

    @SqlUpdate("delete from task where id = :id ")
    void delete(@Bind("id") long id);

    @SqlUpdate("update task set status = :status, updated_at = :updated_at  where id = :id")
    void update_status(@Bind("id") long id, @Bind("status") int status,@Bind("updated_at") Timestamp updated_at);

    @SqlQuery("select * from task where status = :status")
    @UseRowMapper(TaskMapper.class)
    List<Task> by_status(@Bind("status") int status);
}

