package com.dropwizardexample;

import com.dropwizardexample.core.Task;
import com.dropwizardexample.db.TaskDAO;
import com.dropwizardexample.resources.TaskResource;
import io.dropwizard.Application;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import  com.dropwizardexample.health.TemplateHealthCheck;
import com.dropwizardexample.resources.UserResource;
import org.jdbi.v3.core.Jdbi;
import com.dropwizardexample.db.UserDAO;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.db.DataSourceFactory;

public class DropwizardexampleApplication extends Application<DropwizardexampleConfiguration> {

    public static void main(final String[] args) throws Exception {
        new DropwizardexampleApplication().run(args);
        }

    @Override
    public String getName() {
        return "dropwizardexample";
    }

    @Override
    public void initialize(final Bootstrap<DropwizardexampleConfiguration> bootstrap) {
        // TODO: application initialization
        bootstrap.addBundle(new MigrationsBundle<DropwizardexampleConfiguration>() {
            @Override
            public DataSourceFactory getDataSourceFactory(DropwizardexampleConfiguration configuration) {
                return configuration.getDataSourceFactory();
            }
        });
    }

    @Override
    public void run(final DropwizardexampleConfiguration configuration,
                    final Environment environment) {
        final JdbiFactory factory = new JdbiFactory();
        final Jdbi jdbi = factory.build(environment, configuration.getDataSourceFactory(), "mysql");
        final UserDAO userDAO = jdbi.onDemand(UserDAO.class);
        final TaskDAO taskDAO = jdbi.onDemand(TaskDAO.class);
        // TODO: implement application
//        final dropwizardexampleResource resource = new dropwizardexampleResource(
//                configuration.getTemplate(),
//                configuration.getDefaultName()
//        );

        final TemplateHealthCheck healthCheck =
                new TemplateHealthCheck(configuration.getTemplate());
        environment.healthChecks().register("template", healthCheck);
        environment.jersey().register(new UserResource(userDAO));
        environment.jersey().register(new TaskResource(taskDAO));
//        environment.jersey().register(resource);

    }

}
