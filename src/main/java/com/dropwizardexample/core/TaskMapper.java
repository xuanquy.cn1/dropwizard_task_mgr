package com.dropwizardexample.core;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TaskMapper implements RowMapper<Task> {


    @Override
    public Task map( ResultSet resultSet, StatementContext statementContext) throws SQLException {

        Task task = new Task();
        task.setId(resultSet.getLong("id"));
        task.setName(resultSet.getString("name"));
        task.setContent(resultSet.getString("content"));
        task.setUser_id(resultSet.getLong("user_id"));
        task.setDead_line(resultSet.getTimestamp("dead_line"));
        task.setStatus(resultSet.getInt("status"));
        task.setCreated_at(resultSet.getTimestamp("created_at"));
        task.setUpdated_at(resultSet.getTimestamp("updated_at"));
        System.out.println(2);
        return task;
    }
}
