package com.dropwizardexample.core;


import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.util.Objects;

import java.sql.Timestamp;

public class User {

    @JsonProperty
    private long id;

    @JsonProperty
    private String fullname;

    @JsonProperty
    private Timestamp birthday;

    @JsonProperty
    private String jobtitle;

    public void setId(long id) {
        this.id = id;
    }
    public long getId() {
        return id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public Timestamp getBirthday() {
        return birthday;
    }

    public void setBirthday(Timestamp birthday) {
        this.birthday = birthday;
    }

    public String getJobtitle() {
        return jobtitle;
    }

    public void setJobtitle(String jobtitle) {
        this.jobtitle = jobtitle;
    }

    @Override
    public String toString() {
        String str = "";
        str += Long.toString(id);
        str += " " + fullname;
        str += " " + jobtitle;

        return str;
    }
}
