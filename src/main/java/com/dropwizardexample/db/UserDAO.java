package com.dropwizardexample.db;

import com.dropwizardexample.core.User;
import com.dropwizardexample.core.UserMapper;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.statement.UseRowMapper;

import java.sql.Timestamp;
import java.util.List;

public interface UserDAO {

    @SqlQuery("select * from user")
    @UseRowMapper(UserMapper.class)
    List<User> get_all();

    @SqlUpdate("insert into user ( fullname, birthday, jobtitle) values ( :fullname, :birthday, :jobtitle)")
    void insert( @Bind("fullname") String fullname, @Bind("birthday") Timestamp birthday, @Bind("jobtitle") String jobtitle);

    @SqlQuery("select * from user where id = :id")
    @UseRowMapper(UserMapper.class)
    User get(@Bind("id") long id);

    @SqlUpdate("delete from user where id = : id")
    void delete(@Bind("id") long id);



}

